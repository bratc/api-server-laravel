<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_admin = Role::where('name','admin')->first();
        $role_user = Role::where('name','user')->first();

        User::truncate();
        $user = User::create([
            'email' => 'admin@admin.com',
            'password' => Hash::make('123456'),
            'name' => 'Administrator',
        ]);

        $user->role()->attach($role_admin); // Asignación de rol admin

        $user = User::create([
            'email' => 'user@user.com',
            'password' => Hash::make('123456'),
            'name' => 'Usuario',
        ]);

        $user->role()->attach($role_user); // Asignación de rol user
    }
}
