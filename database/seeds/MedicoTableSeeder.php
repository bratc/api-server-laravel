<?php

use Illuminate\Database\Seeder;
use App\Medico;
class MedicoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Medico::truncate();
        Medico::create([
            "medIdentificacion" => "223344",
            "medNombres" => "Juan Camilo",
            "medApellidos" => "Mendoza Llanos",
            "avatar" => "avatar.jpg"
        ]);

        Medico::create([
            "medIdentificacion" => "334455",
            "medNombres" => "Maria Isabel",
            "medApellidos" => "Cardona Mendoza",
            "avatar" => "avatar.jpg"
        ]);
    }
}
