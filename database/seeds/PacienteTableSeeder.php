<?php

use Illuminate\Database\Seeder;
use App\Paciente;
class PacienteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Paciente::truncate();
        Paciente::create([
            "identificacion" => "22334499",
            "nombres" => "Martha Ines",
            "apellidos" => "Molina",
            "fecha_nacimiento" => "1987/09/09",
            "sexo" => "F"
        ]);
    }
}
