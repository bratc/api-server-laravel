<?php

use Illuminate\Database\Seeder;
use App\Cita;
use App\Paciente;
use App\Medico;
class CitaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Cita::truncate();
        $paciente = Paciente::where('identificacion','22334499')->first();
        $medico = Medico::where('medIdentificacion','223344')->first();
        $cita = new Cita();
        $cita->fecha_cita = "2019/08/20";
        $cita->hora_cita = "14:00:00";
        $cita->observaciones = "Nunguna";
        $cita->paciente_id = $paciente->id;
        $cita->medico_id = $medico->id;
        $cita->consultorio_id = 1;
        $cita->save();
    }
}
