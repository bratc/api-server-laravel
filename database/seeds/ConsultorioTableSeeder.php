<?php

use Illuminate\Database\Seeder;

use App\Consultorio;
class ConsultorioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Consultorio::truncate();
        for($i = 0; $i < 3; $i++){
        $consultorio = new Consultorio();
        $consultorio->numero = "10".($i+1);
        $consultorio->nombre = "Consultorio 10".($i+1);
        $consultorio->save();
        }

    }
}
