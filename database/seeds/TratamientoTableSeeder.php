<?php

use Illuminate\Database\Seeder;

use App\Tratamiento;
use App\Paciente;
class TratamientoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Tratamiento::truncate();
        $paciente = Paciente::where('identificacion','22334499')->first();
        $tratamiento = new Tratamiento();
        $tratamiento->descripcion = "Tratamiento para la tos";
        $tratamiento->fecha_inicio = "2019/08/08";
        $tratamiento->fecha_fin = "2019/10/10";
        $tratamiento->paciente_id = $paciente->id;
        $tratamiento->save();
    }
}
