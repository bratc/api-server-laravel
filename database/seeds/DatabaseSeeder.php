<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(MedicoTableSeeder::class);
        $this->call(ConsultorioTableSeeder::class);
        $this->call(PacienteTableSeeder::class);
        $this->call(TratamientoTableSeeder::class);
        $this->call(CitaTableSeeder::class);
    }
}
