<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Paciente;
use App\Medico;
use App\Consultorio;
class Cita extends Model
{
    //
    protected $fillable = [
        'fecha_cita',
        'hora_cita',
        'estado',
        'observaciones',
        'paciente_id',
        'medico_id',
        'consultorio_id'
    ];
    
    /**
     * Relations
     */

    public function paciente() {
        return $this->belongsTo(Paciente::class);
    } 

    public function medico() {
        return $this->belongsTo(Medico::class);
    }

    public function consultorio() {
        return $this->belongsTo(Consultorio::class);
    }
}
