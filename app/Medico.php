<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Cita;
class Medico extends Model
{
    protected $fillable = [
        'medIdentificacion',
        'medNombres',
        'medApellidos',
        'avatar'
    ];

    /**
     * Relations
     */


    public function cita() {
        return $this->belongsTo(Cita::class);
    }

    public function paciente() {
        return $this->belongsToMany(Paciente::class, 'citas');
    }
    public function consultorio() {
        return $this->belongsToMany(Consultorio::class, 'citas');
    }
}
