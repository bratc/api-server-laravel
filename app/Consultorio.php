<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Cita;
class Consultorio extends Model
{
    //
    protected $fillable = [
        'numero',
        'nombre'
    ];

    /**
     * Relations
     */

    public function cita() {
        return $this->hasMany(Cita::class);
    } 
    public function medico() {
        return $this->belongsToMany(Medico::class, 'citas');
    }
}
