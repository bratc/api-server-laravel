<?php

namespace App\Http\Controllers;

use App\Medico;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;

class MedicoController extends Controller
{
    public function __construct()
    {
      //$this->middleware('jwt');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
       // echo "Hola";
       return Medico::with('paciente','consultorio')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $path = $request->file('avatar')->store('public/avatars');
        $url = Storage::url($path);
        $medico = new Medico();
        $medico->avatar = $url;
        $medico->medIdentificacion = $request->input('medIdentificacion');
        $medico->medNombres = $request->input('medNombres');
        $medico->medApellidos = $request->input('medApellidos');
        $medico->save();
        return $medico;
       // return Medico::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Medico  $medico
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return Medico::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Medico  $medico
     * @return \Illuminate\Http\Response
     */
    public function edit(Medico $medico)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Medico  $medico
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $medico = Medico::find($id);
        $path = $request->file('avatar')->store('public/avatars');
        $url = Storage::url($path);
        $medico->avatar = $url;
        $medico->medIdentificacion = $request->input('medIdentificacion');
        $medico->medNombres = $request->input('medNombres');
        $medico->medApellidos = $request->input('medApellidos');
        $medico->update();
        return $medico;  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Medico  $medico
     * @return \Illuminate\Http\Response
     */
    public function destroy(Medico $medico)
    {
        //
    }
/**
     * Remove the specified resource from storage.
     *
     * @param  \App\Medico  $medico
     * @return \Illuminate\Http\Response
     */
    public function getNameByDoc(Request $request){
       return Medico::where('medIdentificacion',$request->input('document'))->first();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Medico  $medico
     * @return \Illuminate\Http\Response
     */
    public function getNameByDocAndApellido($documento, $apellido){
        return Medico::where('medIdentificacion',$documento)->where('medApellidos',$apellido)->first();
     }

     /**
      * get medico by name with query params 
      */

    public function getMedicoByName(Request $request){
       // return $request->all();
       $nombre = $request->query('nombre');
       $apellidos = $request->query('apellido');
       $medico = Medico::where('medNombres','like','%'.$nombre.'%')->where('medApellidos','like','%'.$apellidos.'%')->first();
        if($medico){
            return $medico;
        }
        return ["message" => "No Found"];
    } 
    
    /**
     * get medico by document with query params
     * @return $request
     */

     public function getMedicoBydocument(Request $request, Response $response){
        $documento = $request->query('documento');
        $medico = Medico::where('medIdentificacion',$documento)->first();
        if($medico){
            return $medico;
        }
        return response()->json(['message' => 'Not Found!'], 404);
     }

    /**
     *  Upload file avatar 
     * 
     *  This method uploaded file img to medic
     * 
     *  */
    
     public function uploadFile(Request $request){      
        $medico = Medico::find($request->input('id'));
        $urlAvatar = $medico->avatar;
        $onlyPath = str_replace("storage/","",$urlAvatar);
       
        Storage::delete("/public/".$onlyPath);
        
        $path = $request->file('avatar')->store('public/avatars');
        $url = Storage::url($path);
        $medico->avatar = $url;
        $medico->save(); 
        return $medico;
      
     }
}
