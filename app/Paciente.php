<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Cita;
use App\Tratamiento;
class Paciente extends Model
{
    //
    protected $fillable = [
        "identificacion",
        "nombres",
        "apellidos",
        "fecha_nacimiento",
        "sexo"
    ];

    /**
     * Relations
     */

    public function cita() {
        return $this->hasMany(Cita::class);
    }
    
    public function tratamiento() {
        return $this->hasMany(Tratamiento::class);
    }
    public function medico() {
        return $this->belongsToMany(Medico::class);
    }
}
