<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Paciente;
class Tratamiento extends Model
{
    //
    protected $fillable = [
        "descripcion",
        "fecha_inicio",
        "fecha_fin",
        "paciente_id"
    ];

    /**
     * Relations
     * 
     */

    public function paciente() {
        return $this->belongsTo(Paciente::class);
    }
}
