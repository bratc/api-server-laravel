<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
}); */

Route::group(['prefix' => 'auth'], function () {
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::get('me', 'AuthController@me');
});



Route::group(['middleware' => ['jwt']], function () {

    //Aqui todos los Resources nuevos
    //Protected with middleware

    Route::post('medicos/getname', 'MedicoController@getNameByDoc');
    Route::get('medicos/getnameBydoc/{documento}/{apellido}', 'MedicoController@getNameByDocAndApellido');
    Route::get('medicos/getMedicoByName', 'MedicoController@getMedicoByName');
    Route::get('medicos/get-medico-by-document', 'MedicoController@getMedicoBydocument');

    Route::apiResource('users', 'UserController');
   // Route::apiResource('medicos', 'MedicoController')->middleware('role:admin');
    Route::post('medicos/{id}', 'MedicoController@update')->middleware('role:admin');
    Route::post('medicos/upload-avatar', 'MedicoController@uploadFile');

    
    Route::apiResource('tratamientos', 'TratamientoController');
    Route::apiResource('pacientes', 'PacienteController');
    
});
Route::apiResource('medicos', 'MedicoController');

Route::apiResource('citas', 'CitaController');
Route::apiResource('pacientes', 'PacienteController');

Route::apiResource('roles', 'RoleController');

Route::apiResource('users', 'UserController');
Route::apiResource('consultorios', 'ConsultorioController');